from reader.models import Book


def get_books():
    return [book for book in Book.objects.all()]
