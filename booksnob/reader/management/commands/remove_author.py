from django.core.management.base import BaseCommand, CommandError
#from optparse import make_option
from reader.models import Author


class Command(BaseCommand):
    help = "You can remove author"

    def add_arguments(self, parser):
        parser.add_argument('name', nargs='+', type=str)
        parser.add_argument('--delete',
                            action='store_true',
                            dest='delete',
                            default=False,
                            help='Delete author')

    def handle(self, *args, **kwargs):
        if kwargs['delete']:
            #author = Author.objects.filter(id=int(kwargs['id'][0]))
            #author.delete()
            #print 'Delete author with id: '+str(kwargs['id'])
            author = Author.objects.filter(first_name=str(kwargs['name'][0]))
            author.delete()
            print 'success'
        else:
            raise CommandError("Error no arguments")
