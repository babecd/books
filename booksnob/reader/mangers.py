from django.db import models


class AuthorManager(models.Manager):
    def get_author(self):
        q = super(AuthorManager, self).get_queryset()
        return q.filter(last_name='Lname')
