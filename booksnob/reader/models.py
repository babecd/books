from django.db import models
from reader.mangers import AuthorManager


class Author(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField()
    filter_author = AuthorManager()
    objects = models.Manager()

    def __unicode__(self):
        return '{0} {1}  e-mail: {2}'.format(self.first_name, self.last_name, self.email)


class Book(models.Model):
    author = models.ForeignKey(Author)  # relationship 1:n
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return '{0} -{1}'.format(self.author.first_name, self.name)


class Readers(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    book = models.ManyToManyField(Book)

    def __unicode__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)


class UserAvatar(models.Model):
    user = models.OneToOneField(Author, primary_key=True)
    image = models.ImageField(upload_to='media/', null=True, blank=True)
