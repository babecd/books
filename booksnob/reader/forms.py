from django import forms
from reader.models import Author, UserAvatar


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = '__all__'


class ProfileForm(forms.ModelForm):
    class Meta:
        model = UserAvatar
        fields = ['image']
