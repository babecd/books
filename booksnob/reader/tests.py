from django.test import TestCase
from reader import actions
from reader.models import Author, Book


class ActiontsTestCase(TestCase):
    def setUp(self):
        self.author = Author.objects.create(
            first_name='l',
            last_name='l',
            email='l')
        self.book = Book.objects.create(
            author=self.author,
            name='a')

    def test_get_authors(self):
        authors = actions.get_authors()
        self.assertEqual(authors, [self.author])

    def test_get_books(self):
        books = actions.get_books()
        self.assertEqual(books, [self.book])
