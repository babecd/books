from django.conf.urls import url
#from django.conf.urls import include, url
from reader import views
from django.views.decorators.cache import cache_page


urlpatterns = [

    url(r'^$', cache_page(60*15)(views.HomeView.as_view()), name='home'),
    url(r'^set_auth/$', views.FView.as_view(), name='set_auth'),
    url(r'^thanks/$', views.ThanksView.as_view()),
    url(r'^get_list/(?P<pk>\d+)/$',
        views.GetAuthorView.as_view(), name='author'),
    url(r'^get_list/$', views.GetList.as_view(), name='list_of_authors'),
    url(r'^users/$', views.RegisterFormView.as_view(), name='registration'),
    url(r'^aut/$', views.AuthFormView.as_view(), name='login'),
    url(r'^bye/$', views.LogoutForm.as_view(), name='logout'),
    url(r'^avatar/$', views.DownloadView.as_view()),
    url(r'^time/$', views.set_timezone, name='set_timezone'),
]
