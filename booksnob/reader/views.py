
from django.http import HttpResponse, HttpResponseRedirect
#from django.template.loader import get_template
#from django.template import Context
from django.shortcuts import render
#import datetime
#from django.http import request
from django.views.generic import TemplateView, DetailView, ListView, View, CreateView, UpdateView
from django.views.generic.edit import FormView
from reader.actions import get_books, get_authors, get_all
from reader.models import Author, UserAvatar
from reader.forms import AuthorForm, ProfileForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
#from django.core.cache import cache
from django.shortcuts import redirect
from pytz import common_timezones
from datetime import datetime


class HomeView(TemplateView):
    template_name = 'reader/content.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['authors'] = get_authors()
        context['books'] = get_books()

        return context


class FView(FormView):
    template_name = 'reader/form.html'
    form_class = AuthorForm
    success_url = '/reader/thanks/'

    '''def post(self, request, *args, **kwargs):
        pass'''

    def form_valid(self, form):
        self.object = form.save()
        print self.object
        return super(FView, self).form_valid(form)


def get_author_filter(request):
    #Author.filter_author.get_author()
    #if request.POST:
    form = AuthorForm()
    return HttpResponse(form)


class ThanksView(TemplateView):
    template_name = 'reader/thanks.html'


class GetAuthorView(UpdateView):
    template_name = 'reader/auth.html'
    model = Author
    fields = '__all__'
    success_url = '/reader/thanks/'

    # def post(self, request, *args, **kwargs):
        # print request.POST
        # return super(GetAuthorView, self).post(request, args, kwargs)
    # def form_valid(self, form):
        # form.save()
        # return super(GetAuthorView, self).form_valid(form)


class GetList(ListView):
    template_name = "reader/get_auth.html"
    model = Author

    def post(self, request, *args, **kwargs):
        req = request.POST.get('txtsearch', '')
        some = request.POST.get('filt', '')
        print some
        filter_auth = Author.objects.filter(first_name__startswith=req)
        if some == "up":
            filter_auth = filter_auth.order_by('first_name')
        else:
            filter_auth = filter_auth.order_by('-first_name')
        return render(request, self.template_name, {'author_list': filter_auth})


class RegisterFormView(FormView):
    form_class = UserCreationForm
    template_name = "reader/users.html"
    success_url = "/reader/thanks/"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class AuthFormView(FormView):
    form_class = AuthenticationForm
    template_name = "reader/login.html"
    success_url = "/reader/avatar"

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(AuthFormView, self).form_valid(form)


class LogoutForm(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/reader/aut')


class DownloadView(FormView):
    form_class = ProfileForm
    template_name = 'reader/avatar.html'
    success_url = '/reader/thanks'

    def form_valid(self, form):
        # UserAvatar.objects.get_or_create(user=self.request.user)
        self.object = form.save()
        return super(DownloadView, self).form_valid(form)


def set_timezone(request):
    if request.method == 'POST':
        request.session['django_timezone'] = request.POST['timezone']
        return redirect('/reader')
    else:
        return render(request, 'reader/timezone.html',
                      {'timezones': common_timezones,
                       'TIME_ZONE': 'Europe/Kiev', 'TIME_NOW': datetime.now()})
