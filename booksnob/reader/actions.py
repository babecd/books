from reader.models import Book, Author


def get_books():
    return [book for book in Book.objects.all()]


def get_authors():
    return [author_name for author_name in Author.objects.all()]


def get_all():
    element = {}
    for author_name in Author.objects.all():
        books = [book for book in Book.objects.filter(author=author_name)]
        element[author_name] = books
    print element
    return element
