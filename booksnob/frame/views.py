from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from reader.models import Author
from frame.serializer import AuthorSerialize
from rest_framework.mixins import ListModelMixin, CreateModelMixin, UpdateModelMixin
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAdminUser


class AuthorListAPI(APIView):
    permission_classes = (IsAdminUser,)

    def get(self, request, format=None):
        a = Author.objects.all()
        res = AuthorSerialize(a, many=True)
        return Response(res.data)


class AuthorAPI(APIView):
    permission_classes = (IsAdminUser,)

    def get(self, request, pk=1, format=None):
        a = Author.objects.filter(id=pk)
        res = AuthorSerialize(a, many=True)
        return Response(res.data)

    def post(self, request, pk=1, format=None):
        res = AuthorSerialize(data=request.data, many=True)
        if res.is_valid():
            res.save()
            return Response(res.data)
        return Response(res.data)


class MixinViewApi(ListModelMixin, CreateModelMixin, GenericAPIView):

    queryset = Author.objects.all()
    serializer_class = AuthorSerialize

    def get(self, request, *args, **kwargs):
        """
        Returns information about authors
        """
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Create a new author
        """
        return self.create(request, *args, **kwargs)


class UpdateMixinViewAPI(UpdateModelMixin, GenericAPIView):
        """
        Update information about author
        """
        queryset = Author.objects.all()
        serializer_class = AuthorSerialize

        def put(self, request, *args, **kwargs):
            return self.update(request, *args, **kwargs)
