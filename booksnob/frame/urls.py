from django.conf.urls import include, url
from frame import views

urlpatterns = [
    #url(r'^list/$', views.AuthorListAPI.as_view()),
    #url(r'^list/(?P<pk>\d+)/$', views.AuthorAPI.as_view()),
    url(r'^list/$', views.MixinViewApi.as_view()),
    url(r'^list/(?P<pk>\d+)/$', views.UpdateMixinViewAPI.as_view()),

]
