from rest_framework import routers, serializers, viewsets
from reader.models import Author


class AuthorSerialize(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name']
